class CartItem {
  constructor(product, quantity) {
    this.product = product;
    this.quantity = quantity;
  }
  getItemTotalPrice() {
    return this.product.price * this.quantity;
  }
}

export { CartItem };
