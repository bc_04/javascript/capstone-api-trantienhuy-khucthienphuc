import { RenderUtils } from "../utils/Render.utils.js";

class ProductController {
  #renderObject = "";
  #callBackObject = {};
  constructor() {
    this.#renderObject = new RenderUtils();
  }

  setCallBack(callBackList) {
    this.#callBackObject = { ...callBackList };
  }

  getCallBack() {
    return this.#callBackObject;
  }

  renderProductListHTML(type, target, productData) {
    this.#renderObject.setTargetData(productData);
    this.#renderObject.setTargetEl(target);
    this.#renderObject.setCallBackList(this.getCallBack());
    this.#renderObject.renderHTML(type);
  }

  renderProductDetailModalHTML(type, target, productData, cart = []) {
    // transform data
    if (cart.length) {
      let findIdx = cart.findIndex(
        (cartItem) => cartItem.product.id === productData.id
      );
      if (findIdx > -1) {
        productData["quantityInCart"] = cart[findIdx].quantity;
      } else {
        // product is deleted from cart => Reset to 0
        productData["quantityInCart"] = 0;
      }
    } else {
      productData["quantityInCart"] = 0;
    }
    this.#renderObject.setTargetData(productData);
    this.#renderObject.setTargetEl(target);
    this.#renderObject.setCallBackList(this.getCallBack());
    this.#renderObject.renderHTML(type);
  }

  renderCartTotalItem(cart) {
    let cartItemNumb = document.querySelector(
      ".nav-cart__total-item .item-count"
    );
    cartItemNumb.innerText = cart.length;
  }

  updateProductQuantity(pId, product) {
    // update at product adjust
    console.log("Inside product controller", product);
    document.querySelector(
      `#product-modal .product__content .quantity-adjust[data-pid = '${pId}'] input.product__quantity`
    ).value = product.quantity;
  }

  toggleAdjustButtons(pId) {
    console.log(pId);
    document
      .querySelector(
        `.product-modal .product__content .quantity-control .btn-add-cart[data-pid = '${pId}']`
      )
      .classList.toggle("d-none");
    document
      .querySelector(
        `.product-modal .product__content .quantity-adjust[data-pid = '${pId}']`
      )
      .classList.toggle("d-none");
  }
}

export { ProductController };
