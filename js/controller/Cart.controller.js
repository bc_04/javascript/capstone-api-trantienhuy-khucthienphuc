import { RenderUtils } from "../utils/Render.utils.js";
import { converToNumberWithCommas } from "../utils/System.utils.js";
class CartController {
  #renderObject = "";
  #callBackObject = {};
  constructor() {
    this.#renderObject = new RenderUtils();
  }

  setCallBack(callBackList) {
    this.#callBackObject = { ...callBackList };
  }

  getCallBack() {
    return this.#callBackObject;
  }

  calcCartTotalPrice(cart) {
    return cart.reduce(
      (totalPrice, item) => totalPrice + item.getItemTotalPrice(),
      0
    );
  }
  renderCartProductListHTML(type, target, cart) {
    // prepare cart data
    if (cart.length) {
      cart.totalPrice = this.calcCartTotalPrice(cart);
    }
    this.#renderObject.setTargetData(cart);
    this.#renderObject.setTargetEl(target);
    this.#renderObject.setCallBackList(this.getCallBack());
    this.#renderObject.renderHTML(type);
  }

  renderCheckOutModal(type, target, cartData) {
    if (cartData.length) {
      let cartTotalPrice = cartData.reduce(
        (totalPrice, Item) => totalPrice + Item.getItemTotalPrice(),
        0
      );

      cartData.totalPrice = cartTotalPrice;
    } else {
      cartData.totalPrice = null;
    }
    console.log(cartData);
    this.#renderObject.setTargetData(cartData);
    this.#renderObject.setTargetEl(target);
    this.#renderObject.setCallBackList(this.getCallBack());
    this.#renderObject.renderHTML(type);
  }

  updateCartProductQuantity(pId, cart, idx) {
    let product = cart[idx];
    let targetProductEl = document.querySelector(
      `.cart .cart__details .cart-product-${pId}`
    );
    // update inside popup cart modal
    targetProductEl.querySelector(
      ".quantity-adjust input.product__quantity"
    ).value = product.quantity;

    targetProductEl.querySelector(
      ".product__total-price"
    ).innerText = `${converToNumberWithCommas(product.getItemTotalPrice())} đ`;

    document.querySelector(
      "#cartModal .cart__total-price .number"
    ).innerText = `${converToNumberWithCommas(
      this.calcCartTotalPrice(cart)
    )} đ`;
  }
}

export { CartController };
