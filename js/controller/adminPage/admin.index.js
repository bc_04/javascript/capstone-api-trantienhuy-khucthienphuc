import {
  productControl,
  layThongTinTuForm,
  renderProduct,
  showThongTinLenForm,
  xoaThongTinTrenForm,
  xoaThongBaoLoi,
  validateIdInput,
  validatePriceInput,
  validateInputNoIdNoPrice,
  turnOffloadingIdCheck,
  turnOnLoading,
  turnOffLoading,
  turnOnloadingIdCheck,
  notifyIdCheckStatus,
} from "./admin.product_controller.js";
import { validation } from "../../validator/admin.product_validator.js";
import { enterToActive } from "../../utils/utils.js";

const BASE_URL = "https://62db6ca0d1d97b9e0c4f3326.mockapi.io/";
const PRODUCT_ENDPOINT = "mainProducts/";

// START GLOBAL
// document
//   .getElementById("product-edit")
//   .addEventListener("hidden.bs.modal", () => {
//     document.getElementById("add-product").style.display = "inline-block";
//     document.getElementById("update-product").style.display = "none";
//     xoaThongTinTrenForm();
//     xoaThongBaoLoi();
//   });

let VNProductName = {
  id: "id",
  name: "tên",
  type: "thương hiệu",
  // price: "giá",
  // screen: "màn hình",
  // backCamera: "camera sau",
  // frontCamera: "camera trước",
  // img: "image source",
  // desc: "miêu tả",
};
// END GLOBAL

// START RENDER PRODUCT TO SCREEN
let renderProductFromServer = () => {
  turnOnLoading();
  axios({
    url: BASE_URL + PRODUCT_ENDPOINT,
    method: "GET",
  })
    .then((res) => {
      renderProduct(res.data);
      turnOffLoading();
    })
    .catch((err) => {
      console.log(err);
      turnOffLoading();
    });
};
renderProductFromServer();
// END RENDER PRODUCT TO SCREEN

// START FILTERING PRODUCT
enterToActive(
  ".product .product__search .search__input",
  ".product .product__search .search__button"
);
document.getElementById("search-select").addEventListener("change", (e) => {
  document.querySelector(
    ".product .product__search .search__input"
  ).placeholder = `Nhập ${VNProductName[e.target.value]} sản phẩm cần tìm`;
});
document
  .querySelector(".product .product__search .search__button")
  .addEventListener("click", () => {
    turnOnLoading();
    let searchedValue = document
      .querySelector(".product .product__search .search__input")
      .value.toLowerCase();
    let searchType = document.getElementById("search-select").value;
    let filterProduct = [];
    axios({
      url: BASE_URL + PRODUCT_ENDPOINT,
      method: "GET",
    })
      .then((res) => {
        filterProduct = res.data.filter((product) => {
          return (
            product[searchType].toLowerCase().includes(searchedValue) == true
          );
        });
        renderProduct(filterProduct);
        turnOffLoading();
      })
      .catch((err) => {
        console.log(err);
        turnOffLoading();
      });
  });
// END FILTERING PRODUCT

// START RESET INPUT FIELD
let resetInputField = () => {
  document.getElementById("add-product").style.display = "inline-block";
  document.getElementById("update-product").style.display = "none";
  xoaThongTinTrenForm();
  xoaThongBaoLoi();
  turnOffloadingIdCheck();
};
document
  .getElementById("reset-input-form")
  .addEventListener("click", resetInputField);
// END RESET INPUT FIELD

// START ADDING PRODUCT
document
  .querySelector(".product__add")
  .addEventListener("click", resetInputField);

document.getElementById("add-product").addEventListener("click", () => {
  turnOnLoading();
  let newProduct = layThongTinTuForm();
  //   console.log(newProduct);
  axios({
    url: BASE_URL + PRODUCT_ENDPOINT,
    method: "GET",
  })
    .then((res) => {
      let isValid =
        validateIdInput(newProduct.id, res.data) &
        validatePriceInput(newProduct.price) &
        validateInputNoIdNoPrice(newProduct);
      if (!isValid) {
        turnOffLoading();
        return;
      }
      newProduct.price *= 1;
      axios({
        url: BASE_URL + PRODUCT_ENDPOINT,
        method: "POST",
        data: newProduct,
      })
        .then(() => {
          turnOffloadingIdCheck();
          renderProductFromServer();
          xoaThongTinTrenForm();
        })
        .catch((err) => {
          console.log(err);
          turnOffLoading();
        });
    })
    .catch((err) => {
      console.log(err);
    });
});
// END ADDING PRODUCT

// START EDIT PRODUCT
productControl.edit = (e) => {
  turnOnLoading();
  xoaThongBaoLoi();
  let id = e.target.dataset.id;
  axios({
    url: BASE_URL + PRODUCT_ENDPOINT + id,
    method: "GET",
  })
    .then((res) => {
      turnOffloadingIdCheck();
      showThongTinLenForm(res.data);
      document.getElementById("add-product").style.display = "none";
      document.getElementById("update-product").style.display = "inline-block";
      turnOffLoading();
    })
    .catch((err) => {
      console.log(err);
      turnOffLoading();
    });
};
// END EDIT PRODUCT

// START UPDATE PRODUCT
document.getElementById("update-product").addEventListener("click", (e) => {
  turnOnLoading();
  let updatedProduct = layThongTinTuForm();
  let isValid =
    validatePriceInput(updatedProduct.price) &
    validateInputNoIdNoPrice(updatedProduct);
  if (!isValid) {
    turnOffLoading();
    return;
  }
  updatedProduct.price *= 1;
  axios({
    url: BASE_URL + PRODUCT_ENDPOINT + updatedProduct.id,
    method: "PUT",
    data: updatedProduct,
  })
    .then(() => {
      xoaThongTinTrenForm();
      document.getElementById("add-product").style.display = "inline-block";
      document.getElementById("update-product").style.display = "none";
      renderProductFromServer();
    })
    .catch((err) => {
      console.log(err);
      turnOffLoading();
    });
});
// END UPDATE PRODUCT

// START DELETE PRODUCT
productControl.delete = (e) => {
  turnOnLoading();
  let id = e.target.dataset.id;
  axios({
    url: BASE_URL + PRODUCT_ENDPOINT + id,
    method: "DELETE",
  })
    .then(() => {
      renderProductFromServer();
    })
    .catch((err) => {
      console.log(err);
      turnOffLoading();
    });
};
// END DELETE PRODUCT

// START OBSERVE ID INPUT
let validateIdTimeOut;
document.getElementById("txt-edit-id").addEventListener("input", (e) => {
  clearTimeout(validateIdTimeOut);
  validateIdTimeOut = setTimeout(() => {
    turnOnloadingIdCheck();
    axios({
      url: BASE_URL + PRODUCT_ENDPOINT,
      method: "GET",
    })
      .then((res) => {
        let isValid = validateIdInput(e.target.value, res.data);
        notifyIdCheckStatus(isValid);
      })
      .catch((err) => {
        console.log(err);
        notifyIdCheckStatus(false);
        document.getElementById("txt-edit-id-error").innerText =
          "Lỗi hệ thống, không lấy được thông tin từ server. Vui lòng thử lại sau";
      });
  }, 500);
});
// START OBSERVE ID INPUT

// START OBSERVE PRICE INPUT
let validatePriceTimeOut;
document.getElementById("txt-edit-price").addEventListener("input", (e) => {
  clearTimeout(validatePriceTimeOut);
  validatePriceTimeOut = setTimeout(() => {
    validatePriceInput(e.target.value);
  }, 500);
});
// START OBSERVE PRICE INPUT

// START OBSERVE OTHER INPUT
let validateRegularInputTimeOut;
Array.from(
  document.querySelectorAll("[data-input='input-edit-regular']")
).forEach((element) => {
  element.addEventListener("input", (e) => {
    clearTimeout(validateRegularInputTimeOut);
    validateRegularInputTimeOut = setTimeout(() => {
      let idError = `txt-edit-${e.target.ariaLabel}-error`;
      validation.checkEmpty(
        e.target.value,
        idError,
        e.target.dataset.tiengviet
      );
    }, 200);
  });
});
// END OBSERVE OTHER INPUT
