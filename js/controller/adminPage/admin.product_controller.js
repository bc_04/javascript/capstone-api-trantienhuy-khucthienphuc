import { Product } from "../../model/Product.model.js";
import { hideLongString, numberWithCommas } from "../../utils/utils.js";
import { validation } from "../../validator/admin.product_validator.js";

const productDetails = document.querySelector(".product .product__details");

export let productControl = {
  edit: () => {},
  delete: () => {},
};

// START RENDER PRODUCTS
export let renderProduct = (arrProduct) => {
  productDetails.innerHTML = "";
  arrProduct.forEach((product) => {
    let productTR = document.createElement("tr");
    productTR.classList.add("product__item");
    let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
      product;
    let productContent = `<th>${id}</th>
    <td>${type}</td>
    <td>${name}</td>
    <td>${numberWithCommas(price)}</td>
    <td>${screen}</td>
    <td>${backCamera}</td>
    <td>${frontCamera}</td>
    <td>${hideLongString(img, 30)}</td>
    <td>${hideLongString(desc, 50)}</td>
    <td>
    <span class="fa-regular fa-pen-to-square text-warning" data-id="${id}" data-bs-toggle="modal" data-bs-target="#product-edit"></span>
    <span class="fa-regular fa-trash-can text-danger" data-id="${id}"></span>
    </td>`;
    productTR.innerHTML = productContent;
    productDetails.append(productTR);
    productTR
      .querySelector("td span.fa-trash-can")
      .addEventListener("click", productControl.delete);
    productTR
      .querySelector("td span.fa-pen-to-square")
      .addEventListener("click", productControl.edit);
  });
};
// END RENDER PRODUCTS

// START LẤY THÔNG TIN TỪ FORM
export let layThongTinTuForm = () => {
  let id = document.getElementById("txt-edit-id").value;
  let name = document.getElementById("txt-edit-name").value;
  let price = document.getElementById("txt-edit-price").value;
  let screen = document.getElementById("txt-edit-screen").value;
  let backCamera = document.getElementById("txt-edit-backCam").value;
  let frontCamera = document.getElementById("txt-edit-frontCam").value;
  let img = document.getElementById("txt-edit-img").value;
  let desc = document.getElementById("txt-edit-desc").value;
  let type = document.getElementById("txt-edit-type").value;
  let newProduct = new Product(
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );
  return { ...newProduct, id: id };
};
// END LẤY THÔNG TIN TỪ FORM

// START SHOW THÔNG TIN LÊN FORM
export let showThongTinLenForm = (product) => {
  let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
    product;
  document.getElementById("txt-edit-id").value = id;
  document.getElementById("txt-edit-name").value = name;
  document.getElementById("txt-edit-price").value = price;
  document.getElementById("txt-edit-screen").value = screen;
  document.getElementById("txt-edit-backCam").value = backCamera;
  document.getElementById("txt-edit-frontCam").value = frontCamera;
  document.getElementById("txt-edit-img").value = img;
  document.getElementById("txt-edit-desc").value = desc;
  document.getElementById("txt-edit-type").value = type;
  document.getElementById("txt-edit-id").disabled = true;
};
// END SHOW THÔNG TIN LÊN FORM

// START XÓA THÔNG TIN TRÊN FORM
export let xoaThongTinTrenForm = () => {
  document.getElementById("txt-edit-id").value = "";
  document.getElementById("txt-edit-name").value = "";
  document.getElementById("txt-edit-price").value = "";
  document.getElementById("txt-edit-screen").value = "";
  document.getElementById("txt-edit-backCam").value = "";
  document.getElementById("txt-edit-frontCam").value = "";
  document.getElementById("txt-edit-img").value = "";
  document.getElementById("txt-edit-desc").value = "";
  document.getElementById("txt-edit-type").value = "";
  document.getElementById("txt-edit-id").disabled = false;
};
// END XÓA THÔNG TIN TRÊN FORM

// START THÔNG BÁO LỖI TRÊN FORM
export let xoaThongBaoLoi = () => {
  document.getElementById("txt-edit-id-error").innerText = "";
  document.getElementById("txt-edit-name-error").innerText = "";
  document.getElementById("txt-edit-price-error").innerText = "";
  document.getElementById("txt-edit-screen-error").innerText = "";
  document.getElementById("txt-edit-backCam-error").innerText = "";
  document.getElementById("txt-edit-frontCam-error").innerText = "";
  document.getElementById("txt-edit-img-error").innerText = "";
  document.getElementById("txt-edit-desc-error").innerText = "";
  document.getElementById("txt-edit-type-error").innerText = "";
};
// END THÔNG BÁO LỖI TRÊN FORM

// START VALIDATE FIELD (EXCEPT ID, PRICE)
export let validateInputNoIdNoPrice = (product) => {
  let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
    product;
  return (
    validation.checkEmpty(name, "txt-edit-name-error", "Tên") &
    validation.checkEmpty(screen, "txt-edit-screen-error", "Màn hình") &
    validation.checkEmpty(backCamera, "txt-edit-backCam-error", "Camera sau") &
    validation.checkEmpty(
      frontCamera,
      "txt-edit-frontCam-error",
      "Camera trước"
    ) &
    validation.checkEmpty(img, "txt-edit-img-error", "Nguồn hình") &
    validation.checkEmpty(desc, "txt-edit-desc-error", "Miêu tả") &
    validation.checkEmpty(type, "txt-edit-type-error", "Thương hiệu")
  );
};
// END VALIDATE FIELD (EXCEPT ID AND PRICE)

// START VALIDATE ID
export let validateIdInput = (id, productList) => {
  return (
    validation.checkEmpty(id, "txt-edit-id-error", "id") &&
    validation.checkNumber(id, "txt-edit-id-error", "id") &&
    validation.checkDuplicate(
      id,
      productList,
      "txt-edit-id-error",
      "id đã được sử dụng, vui lòng chọn id khác"
    )
  );
};
// start notify Id check status
export let notifyIdCheckStatus = (isValid) => {
  if (isValid) {
    document.querySelector(
      "#product-edit .edit-id .edit-id__loading .spinner-border"
    ).style.display = "none";
    document.querySelector(
      "#product-edit .edit-id .edit-id__loading .fa-circle-check"
    ).style.display = "inline";
    document.querySelector(
      "#product-edit .edit-id .edit-id__loading .fa-circle-xmark"
    ).style.display = "none";
    return;
  }
  document.querySelector(
    "#product-edit .edit-id .edit-id__loading .spinner-border"
  ).style.display = "none";
  document.querySelector(
    "#product-edit .edit-id .edit-id__loading .fa-circle-check"
  ).style.display = "none";
  document.querySelector(
    "#product-edit .edit-id .edit-id__loading .fa-circle-xmark"
  ).style.display = "inline";
};
// end notify Id check status
// END VALIDATE ID

// START VALIDATE PRICE
export let validatePriceInput = (price) => {
  return (
    validation.checkEmpty(price, "txt-edit-price-error", "Giá") &&
    validation.checkNumber(price, "txt-edit-price-error", "Giá")
  );
};
// END VALIDATE PRICE

// START TURN ON/OFF LOADING WHEN VALIDATING ID
export let turnOnloadingIdCheck = () => {
  document.querySelector(
    "#product-edit .edit-id .edit-id__loading"
  ).style.display = "block";
  document.querySelector(
    "#product-edit .edit-id .edit-id__loading .spinner-border"
  ).style.display = "inline-block";
  Array.from(
    document.querySelectorAll(
      "#product-edit .edit-id .edit-id__loading .fa-regular"
    )
  ).forEach((element) => {
    element.style.display = "none";
  });
};
export let turnOffloadingIdCheck = () => {
  document.querySelector(
    "#product-edit .edit-id .edit-id__loading"
  ).style.display = "none";
};
// END TURN ON/OFF LOADING WHEN VALIDATING ID

// START TURN ON/OFF LOADING SCREEN
export let turnOnLoading = () => {
  document.getElementById("loading-screen").style.display = "flex";
};
export let turnOffLoading = () => {
  document.getElementById("loading-screen").style.display = "none";
};
// END TURN ON/OFF LOADING SCREEN
