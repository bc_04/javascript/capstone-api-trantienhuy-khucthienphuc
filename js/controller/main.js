import { getAllProducts, getProductById } from "../utils/Request.utils.js";
import { Storage } from "../utils/Storage.utils.js";
import { ProductController } from "./Product.controller.js";
import { CartController } from "./Cart.controller.js";
import {
  findNestedItemByKey,
  createUniqueId,
} from "../utils/System.utils.js";
import { CartItem } from "../model/CartItem.model.js";

let productList = [];
let storageObject = new Storage();
const LOCAL_STORAGE_KEY = "cart";
let productControllerObj = new ProductController();
let cartControllerObj = new CartController();
let cart = storageObject.loadData(LOCAL_STORAGE_KEY);
const checkoutModal = new bootstrap.Modal("#checkoutModal");
const cartModal = new bootstrap.Modal("#cartModal");
// convert cart data to Array of CartItem
cart = cart.map((element) => {
  return new CartItem(element.product, element.quantity);
});

let loadProduct = () => {
  // check cart
  getAllProducts()
    .then((res) => {
      productControllerObj.renderProductListHTML(
        "product",
        ".product__list",
        res.data
      );
      cartControllerObj.setCallBack(cartCallBackList);
      cartControllerObj.renderCartProductListHTML(
        "cart",
        ".cart .cart__details",
        cart
      );
      productControllerObj.renderCartTotalItem(cart);
      productList = [...res.data];
    })
    .catch((err) => {
      console.error(err);
    });
};

let showProductModal = (productData) => {
  console.log(cart);
  productControllerObj.renderProductDetailModalHTML(
    "productDetail",
    ".product-modal .product-detail",
    productData,
    cart
  );
};

let filterByBrand = (e) => {
  switch (e.target.value) {
    case "all":
      productControllerObj.renderProductListHTML(
        "product",
        ".product__list",
        productList
      );
      break;
    default:
      let filteredProductList = productList.filter(
        (product) => product.type === e.target.value
      );
      productControllerObj.renderProductListHTML(
        "product",
        ".product__list",
        filteredProductList
      );
  }
};

let addToCart = (e) => {
  let pId = e.target.dataset.pid;
  getProductById(pId)
    .then((res) => {
      cart.unshift(new CartItem(res.data, 1));
      storageObject.deleteData(LOCAL_STORAGE_KEY);
      storageObject.saveData(LOCAL_STORAGE_KEY, cart);
      productControllerObj.renderCartTotalItem(cart);
      updateItemQuantity("product", pId, cart, 0);
      productControllerObj.toggleAdjustButtons(pId);
    })
    .catch((error) => {
      console.error(`Error : ${error}`);
    });
};

let upItemQuantity = (e) => {
  let pId = e.target.dataset.pid;
  let findIdx = findNestedItemByKey(cart, "product", "id", pId);
  try {
    if (findIdx > -1) {
      cart[findIdx].quantity++;
      storageObject.deleteData(LOCAL_STORAGE_KEY);
      storageObject.saveData(LOCAL_STORAGE_KEY, cart);
      // update quantity right at product card
      updateItemQuantity("product", pId, cart, findIdx);
    } else {
      throw `Can't find the product with id ${pId}`;
    }
  } catch (error) {
    console.error(`Error : ${error}`);
  }
};

let updateItemQuantity = (updatePlace, pId, cart, idx) => {
  if (updatePlace.trim().toLowerCase() === "product") {
    productControllerObj.updateProductQuantity(pId, cart[idx]);
  }

  if (updatePlace.trim().toLowerCase() === "cart") {
    // for cart, update quantity => update each item total price and cart total price
    cartControllerObj.updateCartProductQuantity(pId, cart, idx);
  }
};

let downItemQuantity = (e) => {
  let pId = e.target.dataset.pid;

  let findIdx = findNestedItemByKey(cart, "product", "id", pId);
  try {
    if (findIdx > -1) {
      cart[findIdx].quantity--;
      if (cart[findIdx].quantity === 0) {
        cart.splice(findIdx, 1);
        productControllerObj.toggleAdjustButtons(pId);
        productControllerObj.renderCartTotalItem(cart);
      } else {
        updateItemQuantity("product", pId, cart, findIdx);
      }
      storageObject.deleteData(LOCAL_STORAGE_KEY);
      storageObject.saveData(LOCAL_STORAGE_KEY, cart);
    }
  } catch (error) {
    console.error(`Error : ${error}`);
  }
};

let downItemCartQuantity = (e) => {
  console.log(e.target);
  let pId = e.target.dataset.pid;
  let findIdx = findNestedItemByKey(cart, "product", "id", pId);
  try {
    if (findIdx > -1) {
      cart[findIdx].quantity--;
      if (cart[findIdx].quantity === 0) {
        cart.splice(findIdx, 1);
        productControllerObj.renderCartTotalItem(cart);
        cartControllerObj.renderCartProductListHTML(
          "cart",
          ".cart .cart__details",
          cart
        );
        productControllerObj.toggleAdjustButtons(pId);
        storageObject.deleteData(LOCAL_STORAGE_KEY);
      } else {
        updateItemQuantity("cart", pId, cart, findIdx);
        storageObject.deleteData(LOCAL_STORAGE_KEY);
        storageObject.saveData(LOCAL_STORAGE_KEY, cart);
      }
    }
  } catch (error) {
    console.error(`Error : ${error}`);
  }
};

let deleteItemCart = (e) => {
  console.log(e.target);
  let pId = e.target.dataset.pid;
  let findIdx = findNestedItemByKey(cart, "product", "id", pId);
  try {
    if (findIdx > -1) {
      cart[findIdx].quantity === 0;
      cart.splice(findIdx, 1);
      productControllerObj.renderCartTotalItem(cart);
      cartControllerObj.renderCartProductListHTML(
        "cart",
        ".cart .cart__details",
        cart
      );
      storageObject.deleteData(LOCAL_STORAGE_KEY);
      console.log("Cart after delete the product", cart);
    }
    if (cart.length) {
      storageObject.saveData(LOCAL_STORAGE_KEY, cart);
    }
  } catch (error) {
    console.error(`Error : ${error}`);
  }
};

let upItemCartQuantity = (e) => {
  console.log(e.target);
  let pId = e.target.dataset.pid;
  let findIdx = findNestedItemByKey(cart, "product", "id", pId);
  try {
    if (findIdx > -1) {
      cart[findIdx].quantity++;
      storageObject.deleteData(LOCAL_STORAGE_KEY);
      storageObject.saveData(LOCAL_STORAGE_KEY, cart);
      // update quantity right at cart
      updateItemQuantity("cart", pId, cart, findIdx);
    } else {
      throw `Can't find the product with id ${pId}`;
    }
  } catch (error) {
    console.error(`Error : ${error}`);
  }
};

let clearCart = () => {
  cart = [];
  storageObject.deleteData(LOCAL_STORAGE_KEY);
  productControllerObj.renderCartTotalItem(cart);
};
let showCart = () => {
  document
    .querySelector(".ecommerce-nav .nav-cart")
    .addEventListener("click", () => {
      cartControllerObj.renderCartProductListHTML(
        "cart",
        ".cart .cart__details",
        cart
      );
    });
};

let showCheckout = () => {
  cartModal.hide();
  checkoutModal.show();
  cartControllerObj.renderCheckOutModal(
    "checkout",
    ".checkout .checkout__detail",
    cart
  );
};

let productCallBackList = {
  addToCart,
  downItemQuantity,
  upItemQuantity,
  showProductModal,
};

let cartCallBackList = {
  downItemCartQuantity,
  upItemCartQuantity,
  deleteItemCart,
};
productControllerObj.setCallBack(productCallBackList);

loadProduct();
showCart();

document
  .querySelector("#cartModal .btn-checkout")
  .addEventListener("click", showCheckout);

document
  .querySelector(".product__filter .filter-brand #phone-brand")
  .addEventListener("change", filterByBrand);

document
  .querySelector("#checkoutModal .btn-back-cart")
  .addEventListener("click", () => {
    cartModal.show();
    checkoutModal.hide();
  });

document
  .querySelector("#checkoutModal .btn-order")
  .addEventListener("click", () => {
    document.querySelector(
      "#successOrderModal .order-info-content .order-number"
    ).innerText = `#${createUniqueId()}`;
    clearCart();
  });
