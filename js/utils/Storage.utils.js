class Storage {
  constructor() {}

  saveData(storageName = "", data) {
    try {
      if (storageName == "") {
        throw "Invalid keyname for localstoraage";
      } else {
        localStorage.setItem(storageName, JSON.stringify(data));
        console.log(
          `Save data into localstorage with name ${storageName} success`
        );
      }
    } catch (error) {
      console.error(`Error: ${error}`);
    }
  }

  loadData(storageName = "") {
    try {
      if (storageName == "") {
        throw "Invalid keyname for localstoraage";
      } else {
        return JSON.parse(localStorage.getItem(storageName)) || [];
      }
    } catch (error) {
      console.error(`Error: ${error}`);
    }
  }
  deleteData(storageName = "") {
    try {
      if (storageName == "") {
        throw "Invalid keyname for localstoraage";
      } else {
        localStorage.removeItem(storageName);
        console.log(`Delete localstorage with name ${storageName} success`);
      }
    } catch (error) {
      console.error(`Error: ${error}`);
    }
  }
}

export { Storage };
