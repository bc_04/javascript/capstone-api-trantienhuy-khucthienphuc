let findItemByKey = (dataList, key, itemToFind) => {
  try {
    if (dataList === undefined) {
      throw "Data input is invalid, please input an array as dataList";
    }

    if (key === undefined) {
      throw "Data input is invalid, please input the key you want to compare with your data";
    }

    if (itemToFind === undefined) {
      throw "Data input is invalid, please input the data you want to find";
    }
    return dataList.findIndex((item) => item[key] === itemToFind);
  } catch (err) {
    console.error(`Error: ${err}`);
  }
};

let findNestedItemByKey = (dataList = [{}], key1, key2, itemToFind) => {
  try {
    if (dataList === undefined) {
      throw "Data input is invalid, please input an array as dataList";
    }

    if (key1 === undefined || key2 === undefined) {
      throw "Data input is invalid, please input the key you want to compare with your data";
    }

    if (itemToFind === undefined) {
      throw "Data input is invalid, please input the data you want to find";
    }
    return dataList.findIndex((item) => {
      return item[key1][key2] === itemToFind;
    });
  } catch (err) {
    console.error(`Error: ${err}`);
  }
};

let createUniqueId = () => {
  return Math.floor(Math.random() * 90000) + 10000;
};

let converToNumberWithCommas = (num) => {
  // return num.toLocaleString("en-US", { style: "currency", currency: "VND" });
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export { findItemByKey, findNestedItemByKey, createUniqueId, converToNumberWithCommas };
