import { converToNumberWithCommas } from "./System.utils.js";
class RenderUtils {
  #callBackList;
  constructor() {
    this.target = "";
    this.targetData = "";
    this.#callBackList = {};
  }

  setTargetEl(targetEl) {
    this.target = document.querySelector(targetEl);
  }

  getTargetEl() {
    return this.target;
  }

  setTargetData(data) {
    this.targetData = data;
  }

  getTargetData() {
    return this.targetData;
  }

  setCallBackList(callBackList) {
    this.callBackList = { ...callBackList };
  }

  getCallBackList() {
    return this.callBackList;
  }

  clearContent() {
    this.getTargetEl().innerHTML = "";
  }

  /**
   * Create any HTML element by using rest parameter with a document element.
   * Returns the a html dom element.
   * @param el The target object to copy to.
   * @param {...props} Convert any type of props into an object to passs to the target document
   */
  createHTMLElement(el, { ...props }) {
    let htmlEl = document.createElement(el);
    Object.assign(htmlEl, props);
    return htmlEl;
  }

  #createHTMLTemplate(type) {
    if (type === "product") {
      this.#createProductListHTML();
    }

    if (type === "productDetail") {
      this.#createProductDetailModal();
    }

    if (type === "cart") {
      this.#createCartHTML();
    }

    if (type === "checkout") {
      this.#createCheckOutHTML();
    }
  }
  #createProductDetailModal() {
    let { addToCart, downItemQuantity, upItemQuantity } =
      this.getCallBackList();
    let product = this.getTargetData();
    let target = this.getTargetEl();
    let productModalContent = this.createHTMLElement("div", {
      className: "container container-fluid",
    });
    let productQuantityGroup = `
      <div class="btn-group quantity-control">
        <div
          data-pid="${product.id}"
          class="btn btn-add-cart btn-outline-primary d-flex flex--center text-uppercase fw-semibold ${
            product.quantityInCart ? "d-none" : ""
          }"
        >
          <span class="icon-material_shopping_cart icon d-block me-2" data-pid="${
            product.id
          }"></span>
          Add to cart
        </div>
        <div
          class="quantity-adjust ${!product.quantityInCart ? "d-none" : ""}"
          data-incart=${product.quantityInCart}
          data-pid="${product.id}"
        >
            <div class="btn btn-adjust btn-down" data-pid="${product.id}">
              <span class="icon-minus icon" data-pid="${product.id}"></span>
            </div>
            <input type="text" class="product__quantity text-center" value="${
              product.quantityInCart
            }" />
            <div class="btn btn-adjust btn-up" data-pid="${product.id}">
              <span class="icon-plus icon" data-pid="${product.id}"></span>
            </div>
        </div>
      </div>
    `;
    let productImgCol = `
      <div class="col-lg-5 col-md-12 col-xs-12 product__img d-flex flex--center">
        <div class="product__img-wrapper d-flex flex--center">
          <img class="d-block" src="${product.img}" alt="Hinh sp" />
        </div>
      </div>
    `;
    let productDetailCol = `
      <div class="col-lg-7 col-md-12 col-xs-12">
        <div class="product__content h-100 d-flex flex-column">
          <div class="product__info product__heading">
            <div class="stock-status text-uppercase bg--green">
              <span class="text-white fw-semibold">In stock</span>
            </div>
          </div>
          
          <div class="product__info product__name text-capitalize fs-2 fw-semibold">
            <p class="sub-title type text-muted">${product.type}</p>
            <p class="title">${product.name}</p>
          </div>
          <div class="product__info product__desc fs-5">
            <p class="text-muted">
              ${product.desc}
            </p>
          </div>
          <div class="product__info product__feature">
            <ul class="feature__list">
              <li class="feature">
                <span class="feature__name fw-semibold">Màn hình</span>
                <span class="char char--1">:</span>
                <span class="feature-txt">${product.screen}</span>
              </li>
              <li class="feature">
                <span class="feature__name fw-semibold">Camera sau</span>
                <span class="char char--1">:</span>
                <span class="feature-txt">${product.backCamera}</span>
              </li>
              <li class="feature">
                <span class="feature__name fw-semibold">Camera trước</span>
                <span class="char char--1">:</span>
                <span class="feature-txt">${product.frontCamera}</span>
              </li>
            </ul>
          </div>
          <div class="product__info product__price d-flex align-item-center gap-1">
            <p class="price-title fw-semibold fs-5">Price</p>
            <span class="char char--1">:</span>
            <p class="current-price fs-5">${converToNumberWithCommas(
              product.price
            )} đ</p>
          </div>
          ${productQuantityGroup}
        </div>
      </div>
    `;
    productModalContent.innerHTML = `
      <div class="row">
        ${productImgCol} ${productDetailCol}
      </div>
    `;
    target.append(productModalContent);

    document
      .querySelector(
        ".product-modal .product__content .quantity-control .btn-add-cart"
      )
      .addEventListener("click", addToCart);
    document
      .querySelector(
        ".product-modal .product__content .quantity-adjust .btn-down"
      )
      .addEventListener("click", downItemQuantity);
    document
      .querySelector(
        ".product-modal .product__content .quantity-adjust .btn-up"
      )
      .addEventListener("click", upItemQuantity);
  }
  #createProductHTML(product) {
    let productContent = "",
      productHeading = "",
      productImg = "",
      productName = "",
      productDesc = "",
      productFeature = "",
      productPrice = "",
      productHTML = "";

    productContent = this.createHTMLElement("div", {
      className:
        "product__content d-flex flex-column py-3 px-3 justify-content-between",
    });

    productHeading = this.createHTMLElement("div", {
      className: "product__info product__heading",
    });

    productHeading.innerHTML = `
      <div class="stock-status text-uppercase bg--green">
        <span class="text-white fw-semibold">In stock</span>
      </div>
    `;

    productImg = this.createHTMLElement("div", {
      className: "product__info product__img",
    });

    productImg.innerHTML = `
      <div class="product__img-wrapper d-flex flex--center">
        <img src="${product.img}" alt="Hinh sp" />
      </div>
    `;

    productName = this.createHTMLElement("div", {
      className: "product__info product__name text-capitalize fs-4 fw-semibold",
    });

    productName.innerHTML = `
      <p class="title">${product.name}</p>
    `;

    productDesc = this.createHTMLElement("div", {
      className: "product__info product__desc",
    });

    productDesc.innerHTML = `
      <p class="">
        ${product.desc}
      </p>
    `;

    productFeature = this.createHTMLElement("div", {
      className: "product__info product__feature",
    });

    productFeature.innerHTML = `
      <ul class="feature__list">
        <li class="feature">
          <span class="feature__name fw-semibold">Màn hình</span>
          <span class="char char--1">:</span>
          <span class="feature-txt">${product.screen}</span>
        </li>
        <li class="feature">
          <span class="feature__name fw-semibold">Camera sau</span>
          <span class="char char--1">:</span>
          <span class="feature-txt">${product.backCamera}</span>
        </li>
        <li class="feature">
          <span class="feature__name fw-semibold">Camera trước</span>
          <span class="char char--1">:</span>
          <span class="feature-txt">${product.frontCamera}</span>
        </li>
      </ul>
    `;
    productPrice = this.createHTMLElement("div", {
      className: "product__info product__price text-center outlined-1",
    });
    productPrice.innerHTML = `
      <p class="current-price fs-5">${converToNumberWithCommas(
        product.price
      )}</p>
    `;
    productHTML = this.createHTMLElement("div", {
      className: "product col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3",
      id: `product-${product.id}`,
    });

    productContent.append(productHeading);
    productContent.append(productImg);
    productContent.append(productName);
    productContent.append(productDesc);
    productContent.append(productFeature);
    productContent.append(productPrice);
    productHTML.append(productContent);
    return productHTML;
  }
  #createProductListHTML() {
    let productList = this.getTargetData();
    let { showProductModal } = this.getCallBackList();
    productList.forEach((product) => {
      this.getTargetEl().append(this.#createProductHTML(product));
      let currentProductEl = document.querySelector(`#product-${product.id}`);
      currentProductEl.setAttribute("data-bs-toggle", "modal");
      currentProductEl.setAttribute("data-bs-target", "#product-modal");
      currentProductEl.addEventListener("click", () => {
        showProductModal(product);
      });
    });
  }
  #createCartHTML() {
    let { upItemCartQuantity, downItemCartQuantity, deleteItemCart } =
      this.getCallBackList();
    let cartData = this.getTargetData();
    if (cartData.length) {
      document
        .querySelector("#cartModal .modal-content .modal-footer")
        .classList.remove("d-none");
      document.querySelector(".cart__total-price .title").innerText =
        "Total : ";
      document.querySelector(
        ".cart__total-price .number"
      ).innerText = `${converToNumberWithCommas(cartData.totalPrice)} đ`;
      cartData.forEach((cartItem, index) => {
        let rowContainer = this.createHTMLElement("div", {
          className: `row align-items-center my-2 cart-product-${cartItem.product.id} bg-white py-4`,
        });
        let colImg = `
          <div class="col-2">
            <div class="product__img-wrapper d-flex flex--center">
              <img src="${cartItem.product.img}" alt="Hinh sp" />
            </div>
          </div>
        `;
        let colPName = `
          <div class="col-4">
            <div class="product__name fs-6">
              <span class="text-muted text-capitalize">${cartItem.product.type}</span>
              <h5 class="fw-semibold text-capitalize mb-0">${cartItem.product.name}</h5>
            </div>
          </div>
        `;

        let colAdjust = `
          <div class="col-2">
            <div class="quantity-adjust" data-pId="${cartItem.product.id}">
              <div class="btn btn-adjust btn-down" data-pId="${cartItem.product.id}">
                <span class="icon-minus icon" data-pId="${cartItem.product.id}"></span>
              </div>
              <input type="text" class="product__quantity text-center" value="${cartItem.quantity}" />
              <div class="btn btn-adjust btn-up" data-pId="${cartItem.product.id}">
                <span class="icon-plus icon" data-pId="${cartItem.product.id}"></span>
              </div>
            </div>
          </div>
        `;

        let colPrice = `
          <div class="col-3">
            <div class="product__total-price text-center fw-semibold">
              <p>${converToNumberWithCommas(cartItem.getItemTotalPrice())} đ</p>
            </div>
          </div>
        `;

        let colBtnDel = `
        <div class="col-1">
          <div class="btn btn-delete" data-pid="${cartItem.product.id}">
          <span class="icon-trash" data-pid="${cartItem.product.id}"></span>
          </div>
        </div>
        `;
        rowContainer.innerHTML = `${colImg} ${colPName} ${colAdjust} ${colPrice} ${colBtnDel}`;
        this.getTargetEl().append(rowContainer);
        // bind event
        document
          .querySelectorAll(".cart .cart__details .quantity-adjust .btn-down")
          [index].addEventListener("click", downItemCartQuantity);
        document
          .querySelectorAll(".cart .cart__details .quantity-adjust .btn-up")
          [index].addEventListener("click", upItemCartQuantity);
        document
          .querySelectorAll(".cart .cart__details .btn-delete")
          [index].addEventListener("click", deleteItemCart);
      });
    } else {
      let cartEmptyEl = `
      <div class="row my-3 align-items-center">
        <div class="text-empty-cart text-center">
          <p class="text-muted fs-4">You don't have any products inside the cart.</p>
        </div>
      </div>
      `;
      document
        .querySelector(".cart .modal-content .modal-footer")
        .classList.add("d-none");
      this.getTargetEl().innerHTML = cartEmptyEl;
    }
  }
  #createCheckOutHTML() {
    let cartData = this.getTargetData();
    if (cartData.length) {
      let checkoutTotalPriceEl = `
        <div class="total-price d-flex align-items-center justify-content-between fs-4">
          <h4 class="text-uppercase fw-semibold mx-auto">Total</h4>
          <p class="number">${converToNumberWithCommas(
            cartData.totalPrice
          )} đ</p>
        </div>
      `;
      let trHTML = "";
      cartData.forEach((cartItem, idx) => {
        trHTML += `
            <tr>
              <th scope="row">${idx + 1}</th>
              <td class="product__img">
                <img
                  src="${cartItem.product.img}"
                  alt=""
                >
              </td>
              <td class="fs-5 fw-semibold">${cartItem.product.name}</td>
              <td>${cartItem.quantity}</td>
              <td>${converToNumberWithCommas(cartItem.product.price)} đ</td>
              <td>${converToNumberWithCommas(
                cartItem.getItemTotalPrice()
              )} đ</td>
            </tr>
        `;
      });
      this.getTargetEl().innerHTML += `
        <table class="table table-borderless checkout-table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col" class="text-uppercase">image</th>
              <th scope="col" class="text-uppercase">product name</th>
              <th scope="col" class="text-uppercase">quantity</th>
              <th scope="col" class="text-uppercase">unit price</th>
              <th scope="col" class="text-uppercase">total</th>
            </tr>
          </thead>
          <tbody>
            ${trHTML}
          </tbody>
        </table>
        ${checkoutTotalPriceEl}
      `;
    }
  }
  bindEvents(_target, event, callback) {
    if (_target instanceof HTMLElement) {
      _target.addEventListener(event, callback);
    } else {
      document.querySelector(_target).addEventListener(event, callback);
    }
  }

  renderHTML(type = "product") {
    this.clearContent();
    this.#createHTMLTemplate(type);
  }
}

export { RenderUtils };
